%define soversion 3
Name:        openssl
Epoch:       1
Version:     3.0.12
Release:     16
Summary:     Cryptography and SSL/TLS Toolkit
License:     OpenSSL and SSLeay
URL:         https://www.openssl.org/
Source0:     https://www.openssl.org/source/%{name}-%{version}.tar.gz
Source1:     Makefile.certificate

Patch1:      openssl-3.0-build.patch
Patch2:      Backport-aarch64-support-BTI-and-pointer-authentication-in-as.patch
Patch3:      Backport-SM3-acceleration-with-SM3-hardware-instruction-on-aa.patch
Patch4:      Backport-Fix-sm3ss1-translation-issue-in-sm3-armv8.pl.patch
Patch5:      Backport-providers-Add-SM4-GCM-implementation.patch
Patch6:      Backport-SM4-optimization-for-ARM-by-HW-instruction.patch
Patch7:      Backport-Further-acceleration-for-SM4-GCM-on-ARM.patch
Patch8:      Backport-SM4-optimization-for-ARM-by-ASIMD.patch
Patch9:      Backport-providers-Add-SM4-XTS-implementation.patch
Patch10:     Backport-Fix-SM4-CBC-regression-on-Armv8.patch
Patch11:     Backport-Fix-SM4-test-failures-on-big-endian-ARM-processors.patch
Patch12:     Backport-Apply-SM4-optimization-patch-to-Kunpeng-920.patch
Patch13:     Backport-SM4-AESE-optimization-for-ARMv8.patch
Patch14:     Backport-Fix-SM4-XTS-build-failure-on-Mac-mini-M1.patch
Patch15:     Backport-support-decode-SM2-parameters.patch
Patch16:     Feature-support-SM2-CMS-signature.patch
Patch17:     Feature-use-default-id-if-SM2-id-is-not-set.patch
Patch18:     Backport-Make-DH_check_pub_key-and-DH_generate_key-safer-yet.patch
Patch19:     Backport-poly1305-ppc.pl-Fix-vector-register-clobbering.patch
Patch20:     Backport-Limit-the-execution-time-of-RSA-public-key-check.patch
Patch21:     Backport-Add-NULL-checks-where-ContentInfo-data-can-be-NULL.patch
Patch22:     Backport-Fix-SM4-XTS-aarch64-assembly-implementation-bug.patch
Patch23:     fix-add-loongarch64-target.patch
Patch24:     backport-CVE-2024-2511-Fix-unconstrained-session-cache-growth-in-TLSv1.3.patch
Patch25:     backport-Add-a-test-for-session-cache-handling.patch
Patch26:     backport-Extend-the-multi_resume-test-for-simultaneous-resump.patch
Patch27:     backport-Hardening-around-not_resumable-sessions.patch
Patch28:     backport-Add-a-test-for-session-cache-overflow.patch
Patch29:     backport-CVE-2024-4603-Check-DSA-parameters-for-exce.patch
Patch30:     Backport-Add-a-test-for-late-loading-of-an-ENGINE-in-TLS.patch
Patch31:     Backport-Don-t-attempt-to-set-provider-params-on-an-ENGINE-ba.patch
Patch32:     Backport-CVE-2024-4741-Only-free-the-read-buffers-if-we-re-not-using-them.patch
Patch33:     Backport-CVE-2024-4741-Set-rlayer.packet-to-NULL-after-we-ve-finished-using.patch
Patch34:     Backport-CVE-2024-4741-Extend-the-SSL_free_buffers-testing.patch
Patch35:     Backport-CVE-2024-4741-Move-the-ability-to-load-the-dasync-engine-into-sslt.patch
Patch36:     Backport-CVE-2024-4741-Further-extend-the-SSL_free_buffers-testing.patch
Patch37:     Backport-bn-Properly-error-out-if-aliasing-return-value-with-.patch
Patch38:     Backport-CVE-2024-5535-Fix-SSL_select_next_proto.patch
Patch39:     Backport-CVE-2024-5535-Add-a-test-for-ALPN-and-NPN.patch
Patch40:     backport-Add-FIPS_mode-compatibility-macro.patch
Patch41:     Backport-CVE-2024-6119-Avoid-type-errors-in-EAI-related-name-check-logic.patch

Patch42:     backport-Add-CTX-copy-function-for-EVP_MD-to-optimize-the-per.patch
Patch43:     backport-Decoder-resolution-performance-optimizations.patch
Patch44:     backport-Improve-performance-of-the-encoder-collection.patch
Patch45:     backport-evp_md_init_internal-Avoid-reallocating-algctx-if-di.patch
Patch46:     backport-Remove-the-_fetch_by_number-functions.patch
Patch47:     backport-Make-IV-buf-in-prov_cipher_ctx_st-aligned.patch
Patch48:     backport-ossl_namemap_name2_num-Avoid-unnecessary-OPENSSL_str.patch
Patch49:     backport-performance-improve-ossl_lh_strcasehash.patch
Patch50:     backport-01-Improve-FIPS-RSA-keygen-performance.patch
Patch51:     backport-02-Improve-FIPS-RSA-keygen-performance.patch
Patch52:     backport-When-we-re-just-reading-EX_CALLBACK-data-just-get-a-.patch
Patch53:     backport-Avoid-an-unneccessary-lock-if-we-didn-t-add-anything.patch
Patch54:     backport-use-__builtin_expect-to-improve-EVP_EncryptUpdate-pe.patch
Patch55:     backport-Drop-ossl_namemap_add_name_n-and-simplify-ossl_namem.patch
Patch56:     backport-Don-t-take-a-write-lock-to-retrieve-a-value-from-a-s.patch
Patch57:     backport-aes-avoid-accessing-key-length-field-directly.patch
Patch58:     backport-evp-enc-cache-cipher-key-length.patch
Patch59:     backport-Avoid-calling-into-provider-with-the-same-iv_len-or-.patch
Patch60:     backport-property-use-a-stack-to-efficiently-convert-index-to.patch
Patch61:     backport-Revert-Release-the-drbg-in-the-global-default-contex.patch
Patch62:     backport-Refactor-a-separate-func-for-provider-activation-fro.patch
Patch63:     backport-Refactor-OSSL_LIB_CTX-to-avoid-using-CRYPTO_EX_DATA.patch
Patch64:     backport-Release-the-drbg-in-the-global-default-context-befor.patch
Patch65:     backport-params-provide-a-faster-TRIE-based-param-lookup.patch
Patch66:     backport-CVE-2024-13176-Fix-timing-side-channel.patch

Patch9000:   add-FIPS_mode_set-support.patch
Patch9001:   backport-CVE-2024-9143-Harden-BN_GF2m_poly2arr-against-misuse.patch
Patch9002:   Fix-build-error-for-ppc64le.patch

BuildRequires: gcc gcc-c++ perl make lksctp-tools-devel coreutils util-linux zlib-devel
Requires:    coreutils %{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}

%description
OpenSSL is a robust, commercial-grade, and full-featured toolkit for the
Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols.

%package libs
Summary:      A general purpose cryptography library with TLS implementation
Group:        System Environment/Libraries
Requires:     ca-certificates >= 2008-5
Requires:     crypto-policies >= 20180730
Recommends:   openssl-pkcs11%{?_isa}

%description libs
The openssl-libs package contains the libraries that are used
by various applications which support cryptographic algorithms
and protocols.

%package perl
Summary: Perl scripts provided with OpenSSL
Requires: perl-interpreter
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}

%description perl
OpenSSL is a toolkit for supporting cryptography. The openssl-perl
package provides Perl scripts for converting certificates and keys
from other formats to the formats used by the OpenSSL toolkit.

%package devel
Summary:   Development files for openssl
Requires:  %{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}
Requires: pkgconfig

%description devel
%{summary}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info

%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build

sslarch=%{_os}-%{_target_cpu}
%ifarch i686
sslarch=linux-elf
%endif
%ifarch riscv64 loongarch64 ppc64le
sslarch=%{_os}64-%{_target_cpu}
sslflags="--libdir=%{_libdir}"
%endif

%ifarch x86_64 aarch64
sslflags=enable-ec_nistp_64_gcc_128
%endif

RPM_OPT_FLAGS="$RPM_OPT_FLAGS -Wa,--noexecstack -Wa,--generate-missing-build-notes=yes -DPURIFY $RPM_LD_FLAGS"
./Configure \
	--prefix=%{_prefix} --openssldir=%{_sysconfdir}/pki/tls ${sslflags} \
	zlib enable-camellia enable-seed enable-rfc3779 \
	enable-cms enable-md2 enable-rc5 ${ktlsopt} enable-fips\
	no-mdc2 no-ec2m enable-sm2 enable-sm4 enable-buildtest-c++\
	shared  ${sslarch} $RPM_OPT_FLAGS '-DDEVRANDOM="\"/dev/urandom\""' \
	-Wl,--allow-multiple-definition


%make_build all

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
# Install OpenSSL.
install -d $RPM_BUILD_ROOT{%{_bindir},%{_includedir},%{_libdir},%{_mandir},%{_libdir}/openssl,%{_pkgdocdir}}

%make_install

# rename so name with actual version
rename so.%{soversion} so.%{version} $RPM_BUILD_ROOT%{_libdir}/*.so.%{soversion}
# create symbolic link
for lib in $RPM_BUILD_ROOT%{_libdir}/*.so.%{version} ; do
     ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`
     ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`.%{soversion}
done

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs
install -m644 %{SOURCE1} $RPM_BUILD_ROOT%{_pkgdocdir}/Makefile.certificate

mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc/*.pl $RPM_BUILD_ROOT%{_bindir}
mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc/tsget $RPM_BUILD_ROOT%{_bindir}


mkdir -p -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/{certs,crl,newcerts,private}
chmod 700 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/private

touch -r %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/{openssl.cnf,ct_log_list.cnf}


# rename man pages avoid conflicting with other man pages in system
%define manpostfix _openssl
pushd $RPM_BUILD_ROOT%{_mandir}
ln -s -f config.5ossl man5/openssl.cnf.5
for manpage in man*/* ; do
    if [ -L ${manpage} ]; then
        targetfile=`ls -l ${manpage} | awk '{print $NF}'`
        ln -sf ${targetfile}%{manpostfix} ${manpage}%{manpostfix}
        rm -f ${manpage}
    else
        mv ${manpage} ${manpage}%{manpostfix}
    fi
done
popd

# Next step of gradual disablement of ssl3.
# Make SSL3 disappear to newly built dependencies.
sed -i '/^\#ifndef OPENSSL_NO_SSL_TRACE/i\
#ifndef OPENSSL_NO_SSL3\
# define OPENSSL_NO_SSL3\
#endif' $RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf.h

basearch=%{_arch}
%ifarch %{ix86}
basearch=i386
%endif

rm -f $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/*.dist

%check
LD_LIBRARY_PATH=`pwd`${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export LD_LIBRARY_PATH
OPENSSL_ENABLE_MD5_VERIFY=
export OPENSSL_ENABLE_MD5_VERIFY
OPENSSL_SYSTEM_CIPHERS_OVERRIDE=xyz_nonexistent_file
export OPENSSL_SYSTEM_CIPHERS_OVERRIDE
make test || :

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%license LICENSE.txt
%doc NEWS.md README.md
%{_bindir}/openssl
%{_pkgdocdir}/Makefile.certificate

%files libs
%license LICENSE.txt
%dir %{_sysconfdir}/pki/tls
%dir %{_sysconfdir}/pki/tls/certs
%dir %{_sysconfdir}/pki/tls/misc
%dir %{_sysconfdir}/pki/tls/private
%config(noreplace) %{_sysconfdir}/pki/tls/openssl.cnf
%config(noreplace) %{_sysconfdir}/pki/tls/ct_log_list.cnf
%config(noreplace) %{_sysconfdir}/pki/tls/fipsmodule.cnf
%attr(0755,root,root) %{_libdir}/libcrypto.so.%{version}
%{_libdir}/libcrypto.so.%{soversion}
%attr(0755,root,root) %{_libdir}/libssl.so.%{version}
%{_libdir}/libssl.so.%{soversion}
%attr(0755,root,root) %{_libdir}/engines-%{soversion}
%attr(0755,root,root) %{_libdir}/ossl-modules

%files devel
%doc CHANGES.md doc/dir-locals.example.el doc/openssl-c-indent.el
%{_prefix}/include/openssl
%{_libdir}/*.so
%{_libdir}/*.a
%{_mandir}/man3/*
%{_libdir}/pkgconfig/*.pc


%files help
%defattr(-,root,root)
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man7/*
%exclude %{_mandir}/man1/*.pl*
%exclude %{_mandir}/man1/tsget*

%files perl
%{_bindir}/c_rehash
%{_bindir}/*.pl
%{_bindir}/tsget
%{_mandir}/man1/*.pl*
%{_mandir}/man1/tsget*
%dir %{_sysconfdir}/pki/CA
%dir %{_sysconfdir}/pki/CA/private
%dir %{_sysconfdir}/pki/CA/certs
%dir %{_sysconfdir}/pki/CA/crl
%dir %{_sysconfdir}/pki/CA/newcerts

%ldconfig_scriptlets libs

%changelog
* Sat Feb 8 2025 jinlun <jinlun@huawei.com> - 1:3.0.12-16
- fix CVE-2024-13176

* Wed Nov 27 2024 peng.zou <peng.zou@shingroup.cn> - 1:3.0.12-15
- Fix build error for ppc64le

* Tue Nov 26 2024 steven <steven_ygui@163.com> - 1:3.0.12-14
- backport patch for performance improvements

* Fri Nov 22 2024 zhujianwei <zhujianwei7@huawei.com> - 1:3.0.12-13
- backport patch for performance improvements

* Tue Nov 19 2024 wangjinchao <wangjinchao@xfusion.com> - 1:3.0.12-12
- fix broken link

* Thu Oct 17 2024 liningjie <liningjie@xfusion.com> - 1:3.0.12-11
- fix CVE-2024-9143

* Tue Sep 03 2024 yinyongkang <yinyongkang@kylinos.cn> - 1:3.0.12-10
- fix CVE-2024-6119

* Wed Aug 21 2024 fuanan <fuanan3@h-partners.com> - 1:3.0.12-9
- add fips feature

* Fri Jun 28 2024 yinyongkang <yinyongkang@kylinos.cn> - 1:3.0.12-8
- fix CVE-2024-5535

* Mon Jun 24 2024 steven <steven_ygui@163.com> - 1:3.0.12-7
- backport patch

* Mon Jun 3 2024 wangcheng <wangcheng156@huawei.com> - 1:3.0.12-6
- fix CVE-2024-4741

* Fri May 17 2024 cenhuilin <cenhuilin@kylinos.cn> - 1:3.0.12-5
- fix CVE-2024-4603

* Sun Apr 28 2024 wangcheng <wangcheng156@huawei.com> - 1:3.0.12-4
- fix CVE-2024-2511

* Wed Mar  6 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 1:3.0.12-3
- Fix build error for loongarch64

* Thu Jan 18 2024 Xu Yizhou <xuyizhou1@huawei.com> - 1:3.0.12-2
- Fix SM4-XTS aarch64 assembly implementation bug

* Thu Jan 04 2024 wangcheng <wangcheng156@huawei.com> - 1:3.0.12-1
- Upgrade to 3.0.12
  Resolves: CVE-2023-0464
  Resolves: CVE-2023-0465
  Resolves: CVE-2023-0466
  Resolves: CVE-2023-1255
  Resolves: CVE-2023-2650
  Resolves: CVE-2023-5363
  Resolves: CVE-2023-6237
  Resolves: CVE-2023-6129
  Resolves: CVE-2023-5678
  Resolves: CVE-2024-0727

* Fri Sep 22 2023 dongyuzhen <dongyuzhen@h-partners.com> - 1:3.0.9-5
- Backport some upstream patches

* Wed Sep 13 2023 luhuaxin <luhuaxin1@huawei.com> - 1:3.0.9-4
- Support decode SM2 parameters

* Wed Sep 13 2023 luhuaxin <luhuaxin1@huawei.com> - 1:3.0.9-3
- Support SM2 CMS signature and use SM2 default id

* Tue Aug 08 2023 zhujianwei <zhujianwei7@huawei.com> - 1:3.0.9-2
- fix CVE-2023-2975 CVE-2023-3446 CVE-2023-3816

* Sat Jul 22 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.9-1
- upgrade to 3.0.9

* Mon Jun 12 2023 steven <steven_ygui@163.com> - 1:3.0.8-7
- fix CVE-2023-2650

* Wed Apr 26 2023 zcwei <u201911736@hust.edu.cn> - 1:3.0.8-6
- fix CVE-2023-1255

* Tue Apr 4 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.8-5
- fix some CVEs

* Mon Mar 27 2023 xuraoqing <xuraoqing@huawei.com> - 1:3.0.8-4
- fix CVE-2023-0464 and add test cases

* Fri Mar 17 2023 wangjunqiang <wangjunqiang@iscas.ac.cn> - 1:3.0.8-3
- fix sslarch and libdir for riscv64

* Thu Mar 16 2023 Xu Yizhou <xuyizhou1@huawei.com> - 1:3.0.8-2
- backport SM4 GCM/CCM/XTS implementation
- backport SM3/SM4 optimization

* Tue Feb 7 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.8-1
- upgrade to 3.0.8 for fixing CVEs

* Tue Feb 7 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.7-2
- disable sctp in openssl building

* Thu Jan 19 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.7-1
- Package init


